package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    private static final String EXCEPTION_MESSAGE = "cannot build a pyramid from this input";

    private int rows = 1;
    private int cols = 1;

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int[][] resultArray;
        if (isValid(inputNumbers) && isPossibleTriangle(inputNumbers.size())) {
            Collections.sort(inputNumbers);
            resultArray = new int[rows][cols];
            initializeArrayWithZeros(resultArray);
            int center = (cols / 2);
            int digitsInRowCounter = 1;
            int listElementIndex = 0;
            for (int i = 0, offset = 0; i < rows; i++, offset++, digitsInRowCounter++) {
                int start = center - offset;
                for (int j = 0; j < digitsInRowCounter * 2; j += 2, listElementIndex++) {
                    resultArray[i][start + j] = inputNumbers.get(listElementIndex);
                }
            }
        } else {
            throw new CannotBuildPyramidException(EXCEPTION_MESSAGE);
        }
        return resultArray;
    }

    private boolean isPossibleTriangle(int sizeOfInputList) {
        long counter = 0;
        while (counter < sizeOfInputList) {
            counter = counter + rows;
            rows++;
            cols = cols + 2;
        }
        rows = rows - 1;
        cols = cols - 2;
        return sizeOfInputList == counter;
    }

    private boolean isValid(List<Integer> inputList) {
        return !inputList.contains(null);
    }

    private void initializeArrayWithZeros(int[][] array) {
        for (int[] row : array) {
            Arrays.fill(row, 0);
        }
    }
}
