package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        validateInputData(x, y);
        if (x.isEmpty()) {
            return true;
        }
        int xIndex = x.size() - 1;
        for (int i = y.size() - 1; i >= 0; i--) {
            if (y.get(i) != x.get(xIndex)) {
                y.remove(i);
            } else if (xIndex != 0) {
                xIndex--;
            }
        }
        return y.size() == x.size() && y.containsAll(x);
    }

    private void validateInputData(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
    }

}
