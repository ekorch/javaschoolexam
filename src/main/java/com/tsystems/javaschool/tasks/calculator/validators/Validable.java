package com.tsystems.javaschool.tasks.calculator.validators;

public interface Validable {

    boolean validate(String expression);

}
