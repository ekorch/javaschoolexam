package com.tsystems.javaschool.tasks.calculator.validators;

import java.util.ArrayDeque;
import java.util.Deque;

public class ParenthesesValidator implements Validable {

    private static final char L_BRACKET = '(';
    private static final char R_BRACKET = ')';

    @Override
    public boolean validate(String s) {

        Deque<Character> stack = new ArrayDeque<>();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == L_BRACKET) {
                stack.push(L_BRACKET);
            } else if (s.charAt(i) == R_BRACKET) {
                if (stack.isEmpty()) {
                    return false;
                }
                if (stack.pop() != L_BRACKET) {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }

}
