package com.tsystems.javaschool.tasks.calculator;

import com.tsystems.javaschool.tasks.calculator.validators.IsNullOrEmptyValidator;
import com.tsystems.javaschool.tasks.calculator.validators.ParenthesesValidator;
import com.tsystems.javaschool.tasks.calculator.validators.SymbolsValidator;
import com.tsystems.javaschool.tasks.calculator.validators.Validable;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class Calculator {

    private Deque<Double> stack;
    private String convertedStatement;

    public Calculator() {
        stack = new ArrayDeque<>();
        convertedStatement = "";
    }

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (!isValid(statement)) {
            return null;
        }
        StatementConverter converter = new StatementConverter(statement);
        convertedStatement = converter.convertInputString();
        String[] tmpArr = convertedStatement.split(" ");
        for (String currentString : tmpArr) {
            if (currentString.isEmpty())
                return null;
            if (!isOperator(currentString)) {
                stack.push(Double.valueOf(currentString));
            } else {
                double num2 = stack.pop();
                double num1 = stack.pop();
                String calculationResult = makeCalculation(num1, num2, currentString);
                if (calculationResult != null) {
                    stack.push(Double.valueOf(calculationResult));
                } else {
                    return null;
                }
            }
        }
        return formatResult();
    }

    private boolean isValid(String statement) {
        List<Validable> validators = new ArrayList<>();
        validators.add(new IsNullOrEmptyValidator());
        validators.add(new ParenthesesValidator());
        validators.add(new SymbolsValidator());
        for (Validable validator : validators) {
            if (!validator.validate(statement)) {
                return false;
            }
        }
        return true;
    }

    private boolean isOperator(String str) {
        return str.equals("+") || str.equals("-") || str.equals("*") || str.equals("/");
    }

    private String makeCalculation(double num1, double num2, String currentString) {
        switch (currentString) {
            case "-":
                return String.valueOf(num1 - num2);
            case "*":
                return String.valueOf(num1 * num2);
            case "/":
                if (num2 == 0) {
                    return null;
                } else {
                    return String.valueOf(num1 / num2);
                }
            default:
                return String.valueOf(num1 + num2);
        }
    }

    private String formatResult() {
        StringBuilder result = new StringBuilder(String.format("%.4f", stack.pop()).replaceAll(",", "."));
        int zeroCounter = 0;
        for (int i = result.length() - 1; zeroCounter < 4; i--) {
            if (result.charAt(i) == '0') {
                zeroCounter++;
            } else {
                break;
            }
        }
        int lengthCorrection = result.length() - zeroCounter;
        if (zeroCounter == 4) {
            result.setLength(lengthCorrection - 1);
        } else {
            result.setLength(lengthCorrection);
        }
        return String.valueOf(result);
    }
}
