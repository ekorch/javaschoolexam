package com.tsystems.javaschool.tasks.calculator.validators;

public class SymbolsValidator implements Validable {

    @Override
    public boolean validate(String expression) {
        String regex = "^(?=.*[0-9])[-*/.+()0-9]+$";
        return expression.matches(regex) && !expression.contains("..");
    }

}
