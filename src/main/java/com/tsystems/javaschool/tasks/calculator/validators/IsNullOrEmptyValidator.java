package com.tsystems.javaschool.tasks.calculator.validators;

public class IsNullOrEmptyValidator implements Validable {

    @Override
    public boolean validate(String s) {
        return s != null && !s.isEmpty();
    }

}
