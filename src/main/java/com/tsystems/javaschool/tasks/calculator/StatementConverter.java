package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayDeque;
import java.util.Deque;

public class StatementConverter {

    private Deque<Character> stack;
    private String input;
    private StringBuilder output = new StringBuilder();

    public StatementConverter(String inputData) {
        input = inputData;
        stack = new ArrayDeque<>();
    }

    public String convertInputString() {
        for (int j = 0; j < input.length(); j++) {
            char ch = input.charAt(j);
            if (isOperator(ch)) {
                output.append(' ');
                defineOperation(ch, defineOperatorPriority(ch));
                continue;
            }
            isCurrentCharacterNotOperator(ch);
        }
        while (!stack.isEmpty()) {
            output.append(' ');
            output.append(stack.pop());
        }
        return String.valueOf(output);
    }

    private int defineOperatorPriority(char ch) {
        return ch == '+' || ch == '-' ? 1 : 2;
    }

    private boolean isOperator(char ch) {
        return ch == '+' || ch == '-' || ch == '*' || ch == '/';
    }

    private void defineOperation(char operator, int operatorPriority) {
        for (int i = stack.size(); i > 0; i--) {
            char topOperator = stack.pop();
            if (topOperator == '(') {
                stack.push(topOperator);
                break;
            } else {
                int topOperatorPriority = defineOperatorPriority(topOperator);
                if (topOperatorPriority >= operatorPriority) {
                    output.append(topOperator).append(' ');
                } else {
                    stack.push(topOperator);
                }
            }
        }
        stack.push(operator);
    }

    private void getExpressionInBrackets() {
        while (!stack.isEmpty()) {
            char currentChar = stack.pop();
            if (currentChar != '(') {
                output.append(' ');
                output.append(currentChar);
            } else {
                break;
            }
        }
    }

    private void isCurrentCharacterNotOperator(char currentChar) {
        switch (currentChar) {
            case '(':
                stack.push(currentChar);
                break;
            case ')':
                getExpressionInBrackets();
                break;
            default:
                output.append(currentChar);
        }
    }
}
