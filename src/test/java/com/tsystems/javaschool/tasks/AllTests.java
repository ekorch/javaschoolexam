package com.tsystems.javaschool.tasks;

import com.tsystems.javaschool.tasks.calculator.CalculatorTest;
import com.tsystems.javaschool.tasks.pyramid.PyramidBuilderTest;
import com.tsystems.javaschool.tasks.subsequence.SubsequenceTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses(
        {
                CalculatorTest.class,
                PyramidBuilderTest.class,
                SubsequenceTest.class
        })
public class AllTests {
}
